# Docker containers

A set of generic Docker containers that I can use to start up any project.

## Usage

```makefile
make help
make build-alpine-php-latest-cli
make push-alpine-php-latest-cli
```
